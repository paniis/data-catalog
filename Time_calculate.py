from datetime import datetime, timedelta
import calendar
import pandas as pd
import psycopg2 as pg
import warnings
warnings.filterwarnings('ignore')

engine = pg.connect("dbname='postgres' user='postgres' host='localhost' port='5432' password='1234'")
query = "select * FROM test.package"
package = pd.read_sql_query(query,con=engine)
query = "select * FROM test.package_extra"
package_extra = pd.read_sql_query(query,con=engine)
engine.close()

today = datetime.now()

def date_update_frequency(date_,frequency):
    days_in_month = calendar.monthrange(date_.year, date_.month)[1]
    if (date_.day + frequency) > days_in_month:
        month_ += 1
        day = day - days_in_month    
        if month > 12:
            year_ += 1
            month = 1
            date_update = date_.replace(year=year_).replace(month=month_)
            return date_update
        return date_update
    else:
        month_ = (date_.month + frequency)
        date_update = date_.replace(month=month_)
        return date_update
    
def status_color(status):
    if status.days < 0:
        # package.loc[:,'status color'] = 'background-color: red'
        # print(status) # print("green")
        
        status = "เกินเวลาที่กำหนด"
        return status

    elif status.days >= 0 and status.days <= 60:

        # package.loc[:,'status color'] = 'background-color: yellow'  
        # print(status)# print("yellow")
        
        status = "ใกล้เวลาที่กำหนด"
        return status

    elif status.days > 60:
        # package.loc[:,'status color'] = 'background-color: green'
        # print(status) # print("red")
        
        status = "ยังไม่ถึงเวลาที่กำหนด"
        return status
    else:
        pass
        return ""
    
for idx,row in package.iterrows():
    try:
        update_frequency = package_extra.loc[
                package_extra['package_id'].str.contains(row['id']) & 
                package_extra['key'].str.contains("update_frequency_interval|update_frequency_unit")
                ].loc[package_extra['key'] != "update_frequency_unit_other"][['key','value']].set_index('key').T

        package.loc[idx,'update_frequency_unit'] = update_frequency['update_frequency_unit']['value']
        package.loc[idx,'update_frequency_interval'] = update_frequency['update_frequency_interval']['value']

        if update_frequency['update_frequency_unit']['value'] == 'ปี':
            date_next = row['metadata_modified'].replace(year=row['metadata_modified'].date().year+1)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'เดือน':
            # print(row['metadata_modified'].date() + timedelta(mdays[row['metadata_modified'].date().month]))
            date_next = date_update_frequency(row['metadata_modified'],1)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'ไตรมาส':
            date_next = date_update_frequency(row['metadata_modified'],3)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'ครึ่งปี':
            date_next = date_update_frequency(row['metadata_modified'],6)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'สัปดาห์':
            date_next = row['metadata_modified'] + timedelta(weeks=1)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'ชั่วโมง':
            date_next = row['metadata_modified'].replace(hour=row['metadata_modified'].hour +1 )
            # date_next = row['metadata_modified'].replace(hour=next_minute.hour)
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'นาที':
            date_next = row['metadata_modified'].replace(minute=row['metadata_modified'].minute +1 )
            days_left = date_next.date() - today.date()
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == 'วัน':
            date_next = row['metadata_modified'].replace(day=row['metadata_modified'].day+1)
            days_left = date_next - row['metadata_modified']
            status = status_color(days_left)

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == "ตามเวลาจริง":
            date_next = "ตามเวลาจริง"
            days_left = "ตามเวลาจริง"
            status = ""

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == "ไม่ทราบ":
            date_next = "ไม่ทราบ"
            days_left = "ไม่ทราบ"
            status = ""

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        elif update_frequency['update_frequency_unit']['value'] == "ไม่มีการปรับปรุงหลังจากการจัดเก็บข้อมูล":
            date_next = "ไม่มีการปรับปรุงหลังจากการจัดเก็บข้อมูล"
            days_left = "ไม่มีการปรับปรุงหลังจากการจัดเก็บข้อมูล"
            status = ""

            package.loc[idx,'next update'] = date_next
            package.loc[idx,'days left'] = days_left
            package.loc[idx,'status color'] = status
        else:
            pass
            # date_next = row['metadata_modified'].date()
            # days_left = date_next - today

            # package.loc[idx,'next update'] = np.nan
            # package.loc[idx,'days_left'] = np.nan
        # print(idx)
    except:
        print(row['id'])
        # pass
package['days left'] = package['days left'].astype(str).replace('NaT','')
package['next update'] = package['next update'].astype(str).replace('NaT','')

package.to_csv('auto_task.csv',index=False,encoding='utf-8-sig')
